import numpy as np
import sys
import scipy.linalg
import copy
import cantera as ct


class ReactorOde:
    '''
    Class of reactor that handles custom ODEs.

    PACMIND model from CERFACS.
    PhD Thesis Nicolas Barleon, https://www.theses.fr/s214685
    "Detailed modeling and simulations of Nanosecond Repetitively Pulsed
    Discharges for Plasma-Assisted Combustion (PAC)" Universite de Toulouse

    Extension of a previous model developed by Maria Castela et al.
    "Modelling the impact of non-equilibrium discharges on reactive mixtures
    for simulations of plasma-assisted ignition in turbulent flows"
    Combustion and Flame
    '''

    def update_variables(self, t, y):
        '''
        Compute all the variables from the y vector of variables.
        '''

        # State vector is [u, Y_1, Y_2, ... Y_K, evib]
        # rho = m / V = cte in the closed reactor
        self.gas.set_unnormalized_mass_fractions(y[1:1+self.nspecs])
        self.gas.UV = y[0], self.spevol
        self.evib = y[-1]

        if self.pulse_energy_density > 0.:
            self.pvpower = self.compute_pvpower(t)
            (self.psource, self.pvpower_chem, self.pvpower_heat,
             self.pvpower_vib) = self.compute_psource()
            # compute RVT using Castela's approach, Equation (32)
            self.RVT = self.compute_RVT()
        else:
            self.pvpower = 0.
            self.pvpower_vib = 0.
            self.RVT = 0.
            self.psource = np.zeros(self.nspecs)
            self.pvpower_chem = 0.
            self.pvpower_heat = 0.

    def compute_pvpower(self, t):
        '''
        Compute the plasma volumetric power: pvpower [J/m3/s].
        '''
        # transition width = 1/maxgrad = 2 * Kt
        Kt = 5.0e-9 * 0.5

        tpulse = 0.
        pulse_twidth = self.tau_pulse

        # temporal pulse signal
        tsig = 0.5*(np.tanh((t - tpulse)/Kt) - np.tanh((t - tpulse - pulse_twidth)/Kt))

        # normalize so that the integral over one pulse is equal to one
        tsig = tsig/pulse_twidth

        if tsig > 1.e-10:
            self.inpulse = True
        else:
            self.inpulse = False

        pvpower = self.pulse_energy_density * tsig

        return pvpower

    def compute_RVT(self):
        '''
        Compute the relaxation rate of the vibrational energy into total
        energy: RVT [J/m3/s]. Equations (32), (34) and (35) from Castela CNF.
        '''

        tauvt = 0.

        # N2 species
        idx = self.gas.species_index('N2')
        ppress = (self.gas.density*self.gas.Y[idx]*ct.gas_constant
                  /self.gas.molecular_weights[idx]*self.gas.T)
        tauvt += 1./(101325.0/ppress*np.exp(221.0*(self.gas.T**(-1./3.) - 0.029) - 18.42))

        # O2 species
        idx = self.gas.species_index('O2')
        ppress = (self.gas.density*self.gas.Y[idx]*ct.gas_constant
                  /self.gas.molecular_weights[idx]*self.gas.T)
        tauvt += 1./(101325.0/ppress*np.exp(229.0*(self.gas.T**(-1./3.) - 0.0295) - 18.42))

        # O species
        idx = self.gas.species_index('O')
        ppress = (self.gas.density*self.gas.Y[idx]*ct.gas_constant
                  /self.gas.molecular_weights[idx]*self.gas.T)
        tauvt += 1./(101325.0/ppress*np.exp(72.4*(self.gas.T**(-1./3.) - 0.015) - 18.42))

        tauvt = 1.0/tauvt

        # Equation (32)
        # relaxation toward zero!
        # here evib is the "out of equilibrium" vibrational energy
        RVT = self.gas.density*self.evib/tauvt

        return RVT

    def compute_psource(self):
        """
        Compute the plasma source terms Section 10.2 of N. Barleon's
        PhD thesis.
        """
        # init variables
        psource = np.zeros(self.nspecs)
        pvpower_chem = 0.
        pvpower_heat = 0.
        pvpower_vib = 0.
        Qj = np.zeros(self.npro_model)

        # compute molar production rate of the processes
        # Equation (10.33), Qj [kmol/m3/s]
        for j in range(0, self.npro_model):
            Qj[j] = self.alpha_chem[j]*self.pvpower/self.process_energy[j]

        # compute species source terms
        # Equation (10.32) w^p_k = sum_j nu_kj * Q_j [kmol/m3/s]
        for k in range(0, self.nsp_model):
            for j in range(0, self.npro_model):
                psource[self.ispec_model[k]] += self.process_nu[j, k]*Qj[j]

        # compute pvpower_chem, pvpower_heat and pvpower_vib
        # Equation (10.34)
        for j in range(0, self.npro_model):
            pvpower_chem += self.alpha_chem[j]*self.pvpower
        # Equation (10.35)
        pvpower_heat = self.alpha_heat*self.pvpower
        # Equation (10.36)
        pvpower_vib = self.alpha_vib*self.pvpower

        return psource, pvpower_chem, pvpower_heat, pvpower_vib

    def __init__(self, gas, pulse_energy_density, tau_pulse,
                 alpha_heat, alpha_vib, alpha_chem):
        '''Initialize the class.'''

        # cantera check basis
        if gas.basis != 'mass':
            sys.exit('basis = "%s" must be equal to "mass"' % gas.basis)

        # gas object
        # specific volume, constant (closed reactor)
        self.gas = gas
        self.spevol = 1./gas.density

        # useful variables
        self.nspecs = self.gas.n_species

        # flag
        self.inpulse = False

        # initialize pulse parameters
        self.pulse_energy_density = pulse_energy_density
        self.tau_pulse = tau_pulse

        # PACMIND model parameters
        self.alpha_heat = alpha_heat
        self.alpha_vib = alpha_vib
        self.alpha_chem = alpha_chem

        # PACMIND model initialization
        spec_model = ['O2', 'O', 'N2', 'N', 'NO', 'CH4', 'CH3', 'H', 'CO2',
                      'CO', 'H2O', 'OH']
        self.ispec_model = [self.gas.species_index(spec) for spec in spec_model]
        self.nsp_model = 12
        self.npro_model = 7

        self.process_nu = np.zeros([self.npro_model, self.nsp_model])
        # Defined the processes in the model (hardcoded for now)
        #                           O2,    O,   N2,    N,   NO,  CH4,  CH3,    H,  CO2,  CO,   H2O,   OH
        self.process_nu[0, :] = [-1.0,  2.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0]  # O2 -> 2 O
        self.process_nu[1, :] = [ 0.0,  0.0, -1.0,  2.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0]  # N2 -> 2 N
        self.process_nu[2, :] = [-1.0,  0.0, -1.0,  0.0,  2.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0]  # O2 + N2 -> 2 NO
        self.process_nu[3, :] = [ 0.0,  0.0,  0.0,  0.0,  0.0, -1.0,  1.0,  1.0,  0.0,  0.0,  0.0,  0.0]  # CH4 -> CH3 + H
        self.process_nu[4, :] = [ 0.0,  1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0,  1.0,  0.0,  0.0]  # CO2 -> CO + O
        self.process_nu[5, :] = [ 0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0, -1.0,  1.0]  # H2O -> OH + H
        self.process_nu[6, :] = [-0.5,  0.0,  0.0,  0.0,  0.0, -1.0,  1.0,  0.0,  0.0,  0.0,  0.0,  1.0]  # CH4 + 0.5 O2 -> CH3 + OH

        self.process_energy = np.zeros(self.npro_model)
        for i in range(0, self.npro_model):
            for j in range(0, self.nsp_model):
                htform = self.gas.species(spec_model[j]).thermo.h(298.)  # J/kmol
                self.process_energy[i] += self.process_nu[i, j]*htform

        # Global reaction considered:
        process_name = ['O2 -> 2 O', 'N2 -> 2 N', 'O2 + N2 -> 2 NO',
                        'CH4 -> CH3 + H', 'CO2 -> CO + O', 'H2O -> OH + H',
                        'CH4 + 0.5 O2 -> CH3 + OH']
        for i in range(0, self.npro_model):
            print(" The process %s  is considered in the PAC model and\
                   requires %.2f J/mol" % (process_name[i],
                                           self.process_energy[i]*1e-3))

    def __call__(self, t, y):
        '''Return d/dt (u), d/dt (Yk) and d/dt (evib).'''

        # easier var names
        nsp = self.nspecs
        gas = self.gas
        rho = self.gas.density

        # -> update variables from y vector and time
        self.update_variables(t, y)

        # species source terms due to combustion: wdot_plasma_mass [kg/m3/s]
        wdot_comb_mass = gas.net_production_rates*gas.molecular_weights

        # species source terms due to NEP: wdot_plasma_mass [kg/m3/s]
        wdot_plasma_mass = np.zeros(nsp)
        for k in self.ispec_model:
            wdot_plasma_mass[k] = gas.molecular_weights[k]*self.psource[k]

        # species balance equations dYdt = 1/rho*(wdot_comb + wdot_plasma)
        # Equation (12) Castela CNF
        dYdt = np.zeros(nsp)
        dYdt += wdot_comb_mass
        dYdt += wdot_plasma_mass
        dYdt /= rho

        # energy balance equation
        # dudt = 1/rho*(E^p_heat + E^p_chem + R^p_VT)
        # Equation (10) Castela CNF
        dudt = 0.
        dudt += self.pvpower_heat
        dudt += self.pvpower_chem
        dudt += self.RVT
        dudt /= rho

        # mean non-equilibrium vibrational energy balance equation
        # devibdt = 1/rho*(E^p_vib - R^p_VT)
        # Equation (11) Castela CNF
        devibdt = 0.
        devibdt += self.pvpower_vib
        devibdt -= self.RVT
        devibdt /= rho

        return np.hstack((dudt, dYdt, devibdt))
