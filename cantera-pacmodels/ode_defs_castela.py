import numpy as np
import sys
import scipy.linalg
import copy
import cantera as ct


class ReactorOde:
    '''
    Class of reactor that handles custom ODEs.

    Castela's plasma model, https://doi.org/10.1016/j.combustflame.2016.01.009
    Maria Castela et al.
    "Modelling the impact of non-equilibrium discharges on reactive mixtures
    for simulations of plasma-assisted ignition in turbulent flows"
    Combustion and Flame
    '''

    def update_variables(self, t, y):
        '''Compute all the variables from the y vector of variables.'''

        # State vector is [u, Y_1, Y_2, ... Y_K, evib]
        # rho = m / V = cte in the closed reactor
        self.gas.set_unnormalized_mass_fractions(y[1:1+self.nspecs])
        self.gas.UV = y[0], self.spevol
        self.evib = y[-1]

        if self.pulse_energy_density > 0.:
            self.pvpower = self.compute_pvpower(t)
            # Equation (31)
            self.pvpower_vib = (1. - self.alpha)*self.pvpower
            # Equation (32)
            self.RVT = self.compute_RVT()
            # Equations (22), (27) and (28).
            (self.psource_O, self.psource_O2, 
             self.pvpower_chem) = self.compute_pchemsource()
            # Equation (29)
            self.pvpower_heat = self.alpha * self.pvpower - self.pvpower_chem
        else:
            self.pvpower = 0.
            self.pvpower_vib = 0.
            self.RVT = 0.
            self.psource_O = 0.
            self.psource_O2 = 0.
            self.pvpower_chem = 0.
            self.pvpower_heat = 0.

    def compute_pvpower(self, t):
        '''Compute the plasma volumetric power: pvpower [J/m3/s].'''

        # transition width = 1/maxgrad = 2 * Kt
        Kt = 5.0e-9 * 0.5

        tpulse = 0.
        pulse_twidth = self.tau_pulse

        # temporal pulse signal
        tsig = 0.5*(np.tanh((t - tpulse)/Kt) - np.tanh((t - tpulse - pulse_twidth)/Kt))

        # normalize so that the integral over one pulse is equal to one
        tsig = tsig/pulse_twidth

        if tsig > 1.e-10:
            self.inpulse = True
        else:
            self.inpulse = False

        pvpower = self.pulse_energy_density * tsig

        return pvpower

    def compute_RVT(self):
        '''
        Compute the relaxation rate of the vibrational energy into total
        energy: RVT [J/m3/s]. Equations (32), (34) and (35).
        '''

        tauvt = 0.

        # N2 species
        idx = self.gas.species_index('N2')
        ppress = (self.gas.density*self.gas.Y[idx]*ct.gas_constant
                  /self.gas.molecular_weights[idx]*self.gas.T)
        tauvt += 1./(101325.0/ppress*np.exp(221.0*(self.gas.T**(-1./3.) - 0.029) - 18.42))

        # O2 species
        idx = self.gas.species_index('O2')
        ppress = (self.gas.density*self.gas.Y[idx]*ct.gas_constant
                  /self.gas.molecular_weights[idx]*self.gas.T)
        tauvt += 1./(101325.0/ppress*np.exp(229.0*(self.gas.T**(-1./3.) - 0.0295) - 18.42))

        # O species
        idx = self.gas.species_index('O')
        ppress = (self.gas.density*self.gas.Y[idx]*ct.gas_constant
                  /self.gas.molecular_weights[idx]*self.gas.T)
        tauvt += 1./(101325.0/ppress*np.exp(72.4*(self.gas.T**(-1./3.) - 0.015) - 18.42))

        tauvt = 1.0/tauvt

        # Equation (32)
        # relaxation toward zero!
        # here evib is the "out of equilibrium" vibrational energy
        RVT = self.gas.density*self.evib/tauvt

        return RVT

    def compute_pchemsource(self):
        """
        Compute the plasma species source terms: psource_O and psource_O2
        [kmol/m3/s] and the chemical plasma power: pvpower_chem [J/m3/s]
        according to Equations (22), (27) and (28).
        """
        # species indices
        iO = self.iO
        iO2 = self.iO2

        # Equation (27): omega_O = eta * Y_O2 / Y^f_O2 * E^p / e_O [mol/m3/s]
        psource_O = (self.eta*self.gas.Y[iO2]/self.YO2_f*self.pvpower
                     /self.gas.partial_molar_int_energies[iO])

        # Equation (22): omega_O2 = - W_O / W_O2 * omega^p_O
        psource_O2 = (
            -self.gas.molecular_weights[iO]
            /self.gas.molecular_weights[iO2]*psource_O)

        # Equation (28):
        # E^p_chem = eta * Y_O2 / Y^f_O2 * (1 - W_O / W_O2 * e_O2 / e_O) * E^p
        pvpower_chem = (
            self.eta*self.gas.Y[iO2]/self.YO2_f
            *(1.0 - self.gas.molecular_weights[iO]/self.gas.molecular_weights[iO2]
            *self.gas.partial_molar_int_energies[iO2]
            /self.gas.partial_molar_int_energies[iO])
            *self.pvpower)

        return psource_O, psource_O2, pvpower_chem

    def __init__(self, gas, pulse_energy_density, YO2_f,
                 tau_pulse=50e-9, alpha=0.55, eta=0.35):
        '''Initialize the class.'''

        # cantera check basis
        if gas.basis != 'mass':
            sys.exit('basis = "%s" must be equal to "mass"' % gas.basis)

        # gas object
        # specific volume, constant (closed reactor)
        self.gas = gas
        self.spevol = 1./gas.density

        # useful variables
        self.nspecs = self.gas.n_species
        self.iO2 = self.gas.species_index('O2')
        self.iO = self.gas.species_index('O')

        # Castela model parameters
        self.alpha = alpha
        self.eta = eta

        # flag
        self.inpulse = False

        # initialize pulse parameters
        self.pulse_energy_density = pulse_energy_density
        self.YO2_f = YO2_f
        self.tau_pulse = tau_pulse

    def __call__(self, t, y):
        '''Return d/dt (u), d/dt (Yk) and d/dt (evib).'''

        # easier var names
        nsp = self.nspecs
        gas = self.gas
        rho = self.gas.density

        # -> update variables from y vector and time
        self.update_variables(t, y)

        # species source terms due to combustion: wdot_plasma_mass [kg/m3/s]
        wdot_comb_mass = gas.net_production_rates*gas.molecular_weights

        # species source terms due to NEP: wdot_plasma_mass [kg/m3/s]
        wdot_plasma_mass = np.zeros(nsp)
        wdot_plasma_mass[self.iO] = (
            gas.molecular_weights[self.iO]*self.psource_O)
        wdot_plasma_mass[self.iO2] = (
            gas.molecular_weights[self.iO2]*self.psource_O2)

        # species balance equations dYdt = 1/rho*(wdot_comb + wdot_plasma)
        # Equation (12)
        dYdt = np.zeros(nsp)
        dYdt += wdot_comb_mass
        dYdt += wdot_plasma_mass
        dYdt /= rho

        # energy balance equation:
        # dudt = 1/rho*(E^p_heat + E^p_chem + R^p_VT)
        # Equation (10)
        dudt = 0.
        dudt += self.pvpower_heat
        dudt += self.pvpower_chem
        dudt += self.RVT
        dudt /= rho

        # mean non-equilibrium vibrational energy balance equation:
        # devibdt = 1/rho*(E^p_vib - R^p_VT)
        # Equation (11)
        devibdt = 0.
        devibdt += self.pvpower_vib
        devibdt -= self.RVT
        devibdt /= rho

        return np.hstack((dudt, dYdt, devibdt))
