"""
This script needs a modified version of Cantera with custom chemical 
kinetics capability.
"""
import sys
import numpy as np
import pickle
import cantera as ct
import scipy.integrate
import matplotlib as mpl
import matplotlib.pyplot as plt

from ode_defs_pacmind import ReactorOde


cti = 'inputs/reducedS22R225QSS9_0.cti'
gas = ct.Solution(cti)
ct.compile_fortran('inputs/reducedS22R225QSS9_0.f90')

# initial conditions
pressure = 101325.
temperature = 1028.93
composition = 'N2:0.71687462,H2:0.02758362,H:0.00000098,O2:0.12105966,\
               O:0.00001010,H2O:0.06911798,OH:0.00027781,H2O2:0.00000001,\
               HO2:0.00000018,CO:0.00003456,CO2:0.03460326,CH4:0.02968956,\
               NO:0.00074682,N2O:0.00000004,NO2:0.00000081'
gas.TPY = temperature, pressure, composition
evib = 0.

# pulse geometry
radius = 600.e-6
d_gap = 5.e-3
volume = d_gap*np.pi*(radius**2)

alpha_heat = 4.554e-01
alpha_vib = 6.082e-02
alpha_chem = [1.73085e-01, 1.11248e-01, 5.50266e-03, 4.21997e-02, 2.42413e-02,
              1.26818e-01, 6.47288e-04]

# -> initialize the object
pulse_energy = 1.0e-3
pulse_energy_density = pulse_energy/volume
print(" Energy density = %.3e J" % pulse_energy_density)
rho = gas.density
spevol = 1./gas.density

ode = ReactorOde(gas=gas, pulse_energy_density=pulse_energy_density,
                 tau_pulse=50.e-9, alpha_heat=alpha_heat, alpha_vib=alpha_vib,
                 alpha_chem=alpha_chem)
tend = 1.e-3

# -> set initial values
t = 0.
y = np.zeros(2 + gas.n_species)
y[0] = gas.u
y[1:gas.n_species+1] = gas.Y
y[-1] = evib

# -> states
states = ct.SolutionArray(gas, 1, extra={'t': [t], 'evib': [evib]})

print('{:>6} {:>7}'.format('it', 'T'))
solver_type = 'scipy'
nit = 0
end = False
if solver_type == 'scipy':
    solver = scipy.integrate.ode(ode)
    solver.set_integrator('vode', method='bdf', with_jacobian=True)
    solver.set_initial_value(y, t)
    # -> advance in time
    while solver.successful() and not end:
        if solver.t < 300.e-9:
            dt = 1.e-9
        else:
            dt = 1.e-7
        solver.integrate(solver.t + dt)
        nit += 1

        # store every 25 iterations
        if nit % 25 == 0:
            gas.UVY = solver.y[0], spevol, solver.y[1:gas.n_species+1]
            states.append(ode.gas.state, t=solver.t, evib=solver.y[-1])
            print('{:6d} {:7.2f}'.format(nit, gas.T))

        if solver.t > tend:
            end = True

else:
    # -> advance in time using 1 step euler
    while not end:
        # -> evalute dydt = f(t, y) and update ode zone variables
        # with current t, y
        try:
            dydt = ode(t, y)
            if ode.inpulse:
                dt = 1.e-9
            else:
                dt = 1.e-8
            t = t + dt
            y = y + dt*dydt
            nit += 1
        except Exception as e:
            sys.exit(e)

        states.append(ode.gas.state, t=t, evib=ode.evib)

        print('{:6d} {:7.2f}'.format(nit, ode.gas.T))

        if t > tend:
            end = True

# Plot the results if matplotlib is installed.
# See http://matplotlib.org/ to get it.
if '--plot' in sys.argv[1:]:
    f, ax = plt.subplots(2, 4, figsize=(12., 5.))
    # -------
    ax[0, 0].plot(states.t*1e3, states.T)
    ax[0, 0].set_xlabel('Time (ms)')
    ax[0, 0].set_ylabel('Temperature (K)')
    # -------
    ax[0, 1].plot(states.t*1e3, states.Y[:, gas.species_index('O')])
    ax[0, 1].set_xlabel('Time (ms)')
    ax[0, 1].set_ylabel('O Mass Fraction')
    # -------
    ax[0, 2].plot(states.t*1e3, states.Y[:, gas.species_index('OH')])
    ax[0, 2].set_xlabel('Time (ms)')
    ax[0, 2].set_ylabel('OH Mass Fraction')
    # -------
    ax[0, 3].plot(states.t*1e3, states.Y[:, gas.species_index('H')])
    ax[0, 3].set_xlabel('Time (ms)')
    ax[0, 3].set_ylabel('H Mass Fraction')
    # -------
    ax[1, 0].plot(states.t*1e3, states.Y[:, gas.species_index('N')])
    ax[1, 0].set_xlabel('Time (ms)')
    ax[1, 0].set_ylabel('N Mass Fraction')
    # -------
    ax[1, 1].plot(states.t*1e3, states.Y[:, gas.species_index('NO')])
    ax[1, 1].set_xlabel('Time (ms)')
    ax[1, 1].set_ylabel('NO Mass Fraction')
    # -------
    ax[1, 2].plot(states.t*1e3, states.Y[:, gas.species_index('CH3')])
    ax[1, 2].set_xlabel('Time (ms)')
    ax[1, 2].set_ylabel('CH3 Mass Fraction')
    # -------
    ax[1, 3].plot(states.t*1e3, states.Y[:, gas.species_index('CO')])
    ax[1, 3].set_xlabel('Time (ms)')
    ax[1, 3].set_ylabel('CO Mass Fraction')
    # -------
    plt.tight_layout()
    plt.show()
else:
    print("To view a plot of these results, run this script with the option\
           --plot")

states.write_csv('states.csv', cols=('T', 'P', 'Y'))

datadict = {}
datadict['time'] = states.t
datadict['T'] = states.T
datadict['Y'] = {}
for i, spec in enumerate(gas.species_names):
    datadict['Y'][spec] = states.Y[:, i]

with open('states.pickle', 'wb') as handle:
    pickle.dump(datadict, handle)

print('\n')
print('All done!')
