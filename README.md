# pacmodels

pacmodels, constant volume 0-dimensional reactor models for Plasma Assisted Combustion (PAC). 

## Description
The scripts solve for balance equations for energy and species mass fractions and rely on Cantera for the thermochemistry. Chemical reactions from combustion are computed using Cantera. Chemical reactions from non-equilibrium plasma are modelled using the work of Maria Castela [1] or Nicolas Barleon and Lionel Cheng [2].

[1] Maria Castela et al. "Modelling the impact of non-equilibrium discharges on reactive mixtures for simulations of plasma-assisted ignition in turbulent flows", Combustion and Flame, https://doi.org/10.1016/j.combustflame.2016.01.009

[2] Nicolas Barleon "Detailed modeling and simulations of Nanosecond Repetitively Pulsed Discharges for Plasma-Assisted Combustion (PAC)", PhD Thesis, Universite de Toulouse, https://www.theses.fr/s214685

## Installation
You can simply add the path toward cantera-pacmodels to your PYTHONPATH.
Cantera is required for thermochemistry computations.

## Usage
Use examples in the examples folder to get familiar with the code.

